name := """OrderManagement"""
organization := "com.nCinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies += guice
libraryDependencies += "org.pac4j" % "pac4j-mongo" % "3.8.3"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.8.1"
libraryDependencies += "log4j" % "log4j" % "1.2.17"
libraryDependencies += "org.elasticsearch" % "elasticsearch" % "6.5.2"
libraryDependencies += "org.elasticsearch.client" % "transport" % "6.5.2"
libraryDependencies += "org.elasticsearch.client" % "elasticsearch-rest-high-level-client" % "6.5.2"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.4.1"
libraryDependencies += "org.apache.kafka" % "kafka-streams" % "2.4.1"


//libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.13.1"
//libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.13.1"
