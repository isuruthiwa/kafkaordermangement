package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.OrderServiceImpl;

public class OrderController extends Controller {
    private OrderServiceImpl orderService;

    public OrderController() {
        orderService=OrderServiceImpl.getInstance();
    }
    public Result placeOrder(Http.Request request) {
        JsonNode jsonNode=request.body().asJson();
        if(orderService.placeAnOrder(jsonNode))
            return ok("Order added");
        else
            return notAcceptable("Invalid customer or stocks not available");
    }
    public Result getOrdersByCustomer(String id){
        JsonNode jsonNode=orderService.getOrders(Integer.parseInt(id));
        if(jsonNode!=null)
            return ok(jsonNode);
        else
            return notFound();
    }
    public Result updateOrder(Http.Request request) {
        JsonNode jsonNode=request.body().asJson();
        Boolean response=orderService.changeOrderStatus(jsonNode.get("order_id").asInt(),jsonNode.get("orderStatus").asText());
        if(response)
            return ok("Order status updated");
        else
            return notAcceptable("Invalid order status change");
    }
}
