package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import kafka.Consumer;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.ProductServiceImpl;

public class ProductController extends Controller {
    ProductServiceImpl productService;
    Thread consumerThread;
    Consumer customerConsumerLog;

    public ProductController() {
        productService = ProductServiceImpl.getInstance();
        customerConsumerLog = new Consumer("kafkaCustomerLog");
        consumerThread = new Thread(() -> {
            while (true){
                System.out.println(customerConsumerLog.listen());
            }
        });
        consumerThread.start();
    }

    public Result addProduct(Http.Request request) {
        JsonNode jsonNode=request.body().asJson();
        System.out.println(jsonNode);
        productService.addProduct(jsonNode);
        return ok("Product added");
    }

    public Result getProduct(String id){
        JsonNode jsonNode=productService.getProduct(Integer.parseInt(id));
        if(jsonNode!=null)
            return ok(jsonNode);
        else
            return notFound();
    }

    public Result removeProduct(String id){
        if(productService.removeProduct(Integer.parseInt(id)))
            return ok("Success");
        else
            return notFound();
    }

    public Result getAllProducts() {
        return ok(productService.getAllProducts());
    }

}
