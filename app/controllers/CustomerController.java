package controllers;

import bo.Customer;
import com.fasterxml.jackson.databind.JsonNode;
import kafka.Producer;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import service.impl.CustomerServiceImpl;

public class CustomerController extends Controller {
    CustomerServiceImpl customerService;
    Producer kafkaProducer;

    public CustomerController(){
        customerService=CustomerServiceImpl.getInstance();
        kafkaProducer=new Producer("kafkaCustomerLog");
    }

    public Result addCustomer(Http.Request request) {
        JsonNode jsonNode=request.body().asJson();
        kafkaProducer.sendMessage("addCustomer",jsonNode.asText());
        System.out.println(jsonNode);
        customerService.addCustomer(jsonNode);
        return ok("Customer added");
    }

    public Result getCustomer(String id){
        JsonNode jsonNode=customerService.getCustomer(Integer.parseInt(id));
        kafkaProducer.sendMessage("id",id);
        if(jsonNode!=null)
            return ok(jsonNode);
        else
            return notFound();
    }

    public Result getAllCustomers(){
        return ok(customerService.getAllCustomers());
    }

    public Result deleteCustomer(String id){
        if(customerService.deleteCustomer(Integer.parseInt(id)))
            return ok("Success");
        else
            return notFound();
    }
}
