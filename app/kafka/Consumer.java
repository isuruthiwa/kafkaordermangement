package kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Properties;

public class Consumer {
    String topic;
    KafkaConsumer<String,String> kafkaConsumer;

    public Consumer(String topic) {
        this.topic=topic;
        Properties properties = new Properties();
        properties.put("bootstrap.servers","localhost:9092");
        properties.put("group.id","test");
        properties.put("enable.auto.commit","true");
        properties.put("auto.commit.interval.ms","1000");
        properties.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");

        kafkaConsumer=new KafkaConsumer<String, String>(properties);
        kafkaConsumer.subscribe(Collections.singletonList(topic));
    }

    public HashMap<String,String> listen(){
        try {
            while (true) {
                ConsumerRecords<String, String> consumerRecords = kafkaConsumer.poll(1000);
                for (ConsumerRecord consumerRecord : consumerRecords){
                    HashMap<String,String> keyvaluepair= new HashMap<>();
                    keyvaluepair.put(consumerRecord.key().toString(),consumerRecord.value().toString());
                    return keyvaluepair;
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
