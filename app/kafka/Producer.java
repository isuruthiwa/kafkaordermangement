package kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.Arrays;
import java.util.Properties;

public class Producer {

    KafkaProducer<String, String> kafkaProducer;
    String topic;

    public Producer(String topic) {
        this.topic=topic;
        Properties properties = new Properties();
        properties.put("bootstrap.servers","localhost:9092");
        properties.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        kafkaProducer= new KafkaProducer<>(properties);
    }

    public void sendMessage(String messageKey, String message){
        ProducerRecord<String, String> producerRecord=new ProducerRecord<>(this.topic,messageKey,message);
        kafkaProducer.send(producerRecord);
    }

    public void close(){
        kafkaProducer.close();
    }


//
//    public static void main(String[] args) {
//        Producer producer=new Producer("Hello");
//        producer.sendMessage("name","Isuru");
//        Consumer consumer=new Consumer();
//
//        Properties prop = new Properties();
//        prop.put(StreamsConfig.APPLICATION_ID_CONFIG,"word-count");
//        prop.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
//        prop.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//        prop.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//
//        StreamsBuilder builder= new StreamsBuilder();
//        KStream<String,String> textLines= builder.stream("TextLineTopic");
//        KTable<String,Long> wordCounts = textLines
//                .flatMapValues(textLine -> Arrays.asList(textLine.toLowerCase().split("\\W+")))
//                .groupBy((key,word)->word)
//                .count(Materialized.<String,Long, KeyValueStore<Bytes, byte[]>>as("counts-store"));
//        wordCounts.toStream().to("WordsWithCountsTopic", Produced.with(Serdes.String(),Serdes.Long()));
//
//        KafkaStreams streams = new KafkaStreams(builder.build(), prop);
//        streams.start();
//    }
}
