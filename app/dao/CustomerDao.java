package dao;

import bo.Customer;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import connection.MongoConnection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Map;

public class CustomerDao {
    MongoClient mongoClient;
    ArrayList<Customer> customers;
    private static CustomerDao customerDao;
    MongoCollection<Document> customerCollection;

    private CustomerDao(){
        customers=new ArrayList<>();
        mongoClient = MongoConnection.getInstance();
        customerCollection= mongoClient.getDatabase("order_management").getCollection("customers");
        readCustomerCollection();
    }

    public static CustomerDao getInstance(){
        customerDao=customerDao==null?new CustomerDao():customerDao;
        return customerDao;
    }


    public void addCustomer(String name, String addrline1, String addrline2, int streetNo, String landMark, String city,
                            String country, int zipcode, int age, boolean activeStatus){
        Customer customer=new Customer((int) (customerCollection.count()+1),name,addrline1,addrline2,streetNo,landMark,city,country,
                zipcode,age,activeStatus);
        Document customerDocument=new Document();
        customerDocument.put("cust_id", (int) (customerCollection.count() + 1));
        customerDocument.put("name",customer.getName());
        Document addressDocument= new Document();
        addressDocument.put("addrline1",customer.getAddrline1());
        addressDocument.put("addrline2",customer.getAddrline2());
        addressDocument.put("streetNo",customer.getStreetNo());
        addressDocument.put("landMark",customer.getLandMark());
        addressDocument.put("city",customer.getCity());
        addressDocument.put("country",customer.getCountry());
        addressDocument.put("zipcode",customer.getZipcode());
        customerDocument.put("address",addressDocument);
        customerDocument.put("age",customer.getAge());
        customerDocument.put("activeStatus",customer.isActiveStatus());
        customerCollection.insertOne(customerDocument);
        customers.add(customer);
    }

    public void readCustomerCollection(){
        for (Document document : customerCollection.find()) {
            int cust_id=-1;
            String name=null;
            String addrline1=null;
            String addrline2=null;
            int streetNo=-1;
            String landMark=null;
            String city=null;
            String country=null;
            int zipcode=-1;
            int age=-1;
            boolean activeStatus=false;
            for (Map.Entry<String, Object> set : document.entrySet()) {
                if (set.getKey().equals("cust_id")) {
                    cust_id = (int) set.getValue();
                }
                else if (set.getKey().equals("name"))
                    name = (String) set.getValue();
                else if(set.getKey().equals("address")) {
                    Document addressDocument= (Document) set.getValue();
                    for (Map.Entry<String, Object> address : addressDocument.entrySet()) {
                        if (address.getKey().equals("addrline1"))
                            addrline1 = (String) address.getValue();
                        else if (address.getKey().equals("addrline2"))
                            addrline2 = (String) address.getValue();
                        else if (address.getKey().equals("streetNo"))
                            streetNo = (int)address.getValue();
                        else if (address.getKey().equals("landMark"))
                            landMark = (String) address.getValue();
                        else if (address.getKey().equals("city"))
                            city = (String) address.getValue();
                        else if (address.getKey().equals("country"))
                            country = (String) address.getValue();
                        else if (address.getKey().equals("zipcode"))
                            zipcode = (int) address.getValue();
                    }
                }
                else if (set.getKey().equals("age"))
                    age = (int) set.getValue();
                else if (set.getKey().equals("activeStatus"))
                    activeStatus = (boolean) set.getValue();
            }
            if(cust_id!=-1 && name!=null && addrline1!=null && addrline2!=null &&
                    streetNo!=-1 && landMark!=null && city!=null && country!=null &&
                    zipcode!=-1 && age!=-1) {
                customers.add(new Customer(cust_id, name, addrline1, addrline2, streetNo, landMark,
                        city, country, zipcode, age, activeStatus));
            }
        }
    }

    public ArrayList<Customer> getAllCustomers(){
        return customers;
    }

    public Customer getCustomer(int cust_id){
        for(Customer customer:customers){
            if(customer.getCust_id()==cust_id){
                return customer;
            }
        }
        return null;
    }

    public boolean removeCustomer(int cust_id){
        customerCollection.deleteOne(Filters.eq("cust_id",cust_id));
        return customers.remove(getCustomer(cust_id));
    }

    public boolean isExistingAndActiveCustomer(int cust_id){
        for(Customer customer:customers){
            if(customer.getCust_id()==cust_id) {
                return customer.isActiveStatus();
            }
        }
        return false;
    }


}
