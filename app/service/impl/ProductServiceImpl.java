package service.impl;

import bo.Product;
import com.fasterxml.jackson.databind.JsonNode;
import dao.ProductDao;
import org.apache.log4j.Logger;
import play.libs.Json;
import service.ProductService;

public class ProductServiceImpl implements ProductService {
    private static ProductServiceImpl productService;
    private ProductDao productDao;

    private static Logger log;

    private ProductServiceImpl(){
        productDao=ProductDao.getInstance();
        log = Logger.getLogger(ProductServiceImpl.class.getName());
    }

    public static ProductServiceImpl getInstance(){
        productService=productService==null?new ProductServiceImpl():productService;
        return productService;
    }

    @Override
    public void addProduct(JsonNode jsonNode) {
        productDao.addProduct(jsonNode.get("productName").asText(),jsonNode.get("stockQuantity").asInt(),jsonNode.get("pricePerUnit").asDouble());
        log.info(" Product added :"+jsonNode.get("productName").asText());
    }

    @Override
    public boolean removeProduct(int product_id) {
        if(productDao.removeProduct(product_id)){
            log.info(" Product to be removed : product id "+product_id);
            return true;
        }
        else{
            log.error(" Product to be removed : failed for product id"+product_id);
            return false;
        }
    }

    @Override
    public JsonNode getAllProducts() {
        log.info(" All products retrieved at ");
        return Json.toJson(productDao.getAllProducts());
    }

    @Override
    public JsonNode getProduct(int id) {
        Product product=productDao.getProduct(id);
        if(product!=null) {
            log.info(" Product details retrieved for : product id "+id);
            return Json.toJson(product);
        }else{
            log.error(" Product details retrieval failed : invalid product id ("+id+")");
            return null;
        }
    }
}
