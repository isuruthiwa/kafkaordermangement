package service.impl;

import bo.Order;
import bo.OrderStatus;
import com.fasterxml.jackson.databind.JsonNode;
import dao.CustomerDao;
import dao.OrderDao;
import dao.ProductDao;
import org.apache.log4j.Logger;
import play.libs.Json;
import service.OrderService;
import java.util.ArrayList;
import java.util.HashMap;

public class OrderServiceImpl implements OrderService {
    private static OrderServiceImpl orderService;
    private OrderDao orderDao;
    private CustomerDao customerDao;
    private ProductDao productDao;

    private static Logger log;

    private OrderServiceImpl(){
        orderDao=OrderDao.getInstance();
        customerDao=CustomerDao.getInstance();
        productDao=ProductDao.getInstance();
        log = Logger.getLogger(OrderServiceImpl.class.getName());
    }

    public static OrderServiceImpl getInstance(){
        orderService=orderService==null?new OrderServiceImpl():orderService;
        return orderService;
    }

    @Override
    public boolean placeAnOrder(JsonNode jsonNode) {
        int cust_id=jsonNode.get("cust_id").asInt();
        if(customerDao.isExistingAndActiveCustomer(cust_id)) {
            HashMap<Integer, Integer> products = new HashMap<>();
            for (JsonNode node : jsonNode.get("products")) {
                int product_id=node.get("product_id").asInt();
                int quantity=node.get("quantity").asInt();
                if(!productDao.isStocksAvailable(product_id,quantity)) {
                    log.error(" Order placement failed : no stock available for "+product_id);
                    return false;
                }
                products.put(product_id, quantity);
            }
            productDao.reduceAvailableStocks(products);
            log.info(" Order placement successful : for customer "+cust_id);
            return orderDao.addOrder(products, jsonNode.get("cust_id").asInt(),
                    jsonNode.get("date").asText(), jsonNode.get("orderStatus").asText());
        }
        else {
            log.error(" Order placement failed : Invalid customer "+jsonNode.get("name").asText());
            return false;
        }
    }

    @Override
    public boolean changeOrderStatus(int order_id, String orderStatus) {
        if(orderDao.updateOrderStatus(order_id, OrderStatus.valueOf(orderStatus))){
            log.info(" Order status change to : "+orderStatus+" for order id "+order_id);
            return true;
        }else{
            log.error(" Order status change to : "+orderStatus+" failed for order id "+order_id);
            return false;
        }
    }

    @Override
    public JsonNode getOrders(int cust_id) {
        ArrayList<Order> orderArrayList=orderDao.getOrders(cust_id);
        if(orderArrayList.isEmpty()) {
            log.error(" Get order list failed: no orders for "+cust_id);
            return null;
        }
        else {
            log.info(" Get order list success: customer id "+cust_id);
            return Json.toJson(orderArrayList);
        }
    }
}
