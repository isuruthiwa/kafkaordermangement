package service.impl;

import bo.Customer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Singleton;
import dao.CustomerDao;
import org.apache.log4j.Logger;
import play.libs.Json;
import service.CustomerService;

@Singleton
public class CustomerServiceImpl implements CustomerService {
    private static CustomerServiceImpl customerService;
    private CustomerDao customerDao;

    private static Logger log;

    private CustomerServiceImpl(){
        customerDao=CustomerDao.getInstance();
        log = Logger.getLogger(CustomerServiceImpl.class.getName());
    }

    public static CustomerServiceImpl getInstance(){
        customerService=customerService==null?new CustomerServiceImpl():customerService;
        return customerService;
    }


    @Override
    public void addCustomer(JsonNode jsonNode) {
        //Validation
        customerDao.addCustomer(jsonNode.get("name").asText(),jsonNode.get("address").get("addrline1").asText(),jsonNode.get("address").get("addrline2").asText(),
                jsonNode.get("address").get("streetNo").asInt(),jsonNode.get("address").get("landMark").asText(),jsonNode.get("address").get("city").asText(),jsonNode.get("address").get("country").asText(),
                jsonNode.get("address").get("zipcode").asInt(),jsonNode.get("age").asInt(),jsonNode.get("activeStatus").asBoolean());
        log.info(" Customer added :"+jsonNode.get("name").asText());
    }

    @Override
    public boolean deleteCustomer(int cust_id) {
        if(customerDao.removeCustomer(cust_id)){
            log.info(" Customer deleted : customer id is "+cust_id);
            return true;
        }
        else {
            log.error(" Customer deleting failed : customer id is "+cust_id);
            return false;
        }
    }

    @Override
    public JsonNode getCustomer(int id) {
        Customer customer=customerDao.getCustomer(id);
        if(customer!=null) {
            log.info(" Customer data retrieved : For customer id "+id);
            return Json.toJson(customer);
        }else {
            log.error(" Customer data retrieval failed : no such customer with id "+id);
            return null;
        }
    }

    @Override
    public JsonNode getAllCustomers() {
        log.info(" All Customer data retrieved");
        return Json.toJson(customerDao.getAllCustomers());
    }
}
