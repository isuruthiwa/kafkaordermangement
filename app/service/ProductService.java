package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface ProductService {
    void addProduct(JsonNode jsonNode);
    boolean removeProduct(int product_id);
    JsonNode getAllProducts();
    JsonNode getProduct(int id);
}
