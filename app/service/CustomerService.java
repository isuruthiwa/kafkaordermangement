package service;

import com.fasterxml.jackson.databind.JsonNode;

public interface CustomerService {
    void addCustomer(JsonNode jsonNode);
    boolean deleteCustomer(int cust_id);
    JsonNode getCustomer(int id);
    JsonNode getAllCustomers();

}
