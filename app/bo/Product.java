package bo;

public class Product {
    private int product_id;
    private String productName;
    private int stockQuantity;
    private double pricePerUnit;

    public Product(int product_id, String productName, int stockQuantity, double pricePerUnit) {
        this.product_id = product_id;
        this.productName = productName;
        this.stockQuantity = stockQuantity;
        this.pricePerUnit = pricePerUnit;
    }

    public int getProduct_id() {
        return product_id;
    }

    public String getProductName() {
        return productName;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public double getPricePerUnit() {
        return pricePerUnit;
    }

    public void reduceAvailableStock(int stockOrder){
        this.stockQuantity-=stockOrder;
    }

    @Override
    public String toString() {
        return "Product{" +
                "product_id=" + product_id +
                ", productName='" + productName + '\'' +
                ", stockQuantity=" + stockQuantity +
                ", pricePerUnit=" + pricePerUnit +
                '}';
    }
}
