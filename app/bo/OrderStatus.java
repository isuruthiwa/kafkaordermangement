package bo;

public enum  OrderStatus {
    PENDING,
    IN_PROGRESS,
    DELIVERED,
    CANCELLED
}
